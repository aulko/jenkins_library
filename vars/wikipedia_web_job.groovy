// import testrail.TestrailMethod
import zephyr_scale.ZephyrScaleMethod
// import common.CommonMethod

/* groovylint-disable-next-line MethodReturnTypeRequired, NoDef */
def call(Object jobSettings) {
    // TestrailMethod trMethod = new TestrailMethod(this)
    // CommonMethod commonMethod = new CommonMethod(this)

    ZephyrScaleMethod zsMethod = new ZephyrScaleMethod(this)

    TimeZone tz = TimeZone.getTimeZone('Europe/Kiev')
    GString date = "[${new Date().format('MM.dd.yyyy-HH:mm', tz)}]"
    GString name = "${date} ${jobSettings.name} for browsers (${jobSettings.browsers})"
    ArrayList browsers = jobSettings.browsers.split(',')
    // Object trConfigs = commonMethod.CutObjectValueByList(jobSettings.testrail.browserConfigs, browsers)
    // Object trConfigIds = commonMethod.getValuesFromObject(trConfigs)
    // Object trRuns
    // String trPlanUrl

    pipeline {
        agent { node { label jobSettings['node'] } }
        options {
            buildDiscarder(logRotator(daysToKeepStr: '30', numToKeepStr: '20'))
            ansiColor('xterm')
        }
        environment {
            JENKINS_UID = sh(script:'id -u', returnStdout: true).trim()
            JENKINS_GID = sh(script:'id -g', returnStdout: true).trim()
        }
        stages {
            // stage('Preparing Testrail') {
            //     steps {
            //         script {
            //             String trProjectId = jobSettings.testrail.projectId
            //             String trSubMilestoneName = jobSettings.name
            //             String trSuiteId = jobSettings.testrail.suiteId
            //             String trCaseTypesId = jobSettings.testrail.caseTypeIds
            //             String trMilestoneId
            //             String trMilestoneName
            //             String trPlanId
            //             (trMilestoneId, trMilestoneName) = trMethod.getLastMilestoneNotComplete(trProjectId)

            //             String trSubMilestoneId = trMethod.getSubMilestone(trProjectId, trMilestoneId,
            //                                                                trSubMilestoneName)

            //             (trPlanId, trPlanUrl) = trMethod.addPlan(trProjectId, trSubMilestoneId, name)
            //             trRuns = trMethod.createEntryWithRunsCases(trProjectId, trSuiteId, trPlanId, trConfigIds,
            //                                                        trCaseTypesId)
            //             echo"${trRuns}"
            //         }
            //     }
            // }

            stage('Preparing Zephyr Scale') {
                steps {
                    script {
                        String zsProjectKey = 'ST'
                        String zsTestTypeFolderName = jobSettings.name
                        String zsTestcaseTypeName = 'Smoke'
                        String zsTestcasesFolderName = 'WIKIPEDIA-WEB'

                        String zsLastReleaseFolderId = zsMethod.getLastReleaseFolder()
                        echo"${zsLastReleaseFolderId}"

                        String zsTestTypeFolderId = zsMethod.getTestTypeFolder(
                            zsProjectKey, zsLastReleaseFolderId, zsTestTypeFolderName)
                        echo"${zsTestTypeFolderId}"

                        String zsPlanFolderId = zsMethod.createPlanFolder(
                            zsProjectKey, zsTestTypeFolderId, name)
                        echo"${zsPlanFolderId}"

                        Object zsPlans = zsMethod.addTestCycle(zsProjectKey, zsPlanFolderId, browsers)
                        echo"${zsPlans}"
                        zsPlans.each { zsPlan -> 
                            zsMethod.createExecutionWithTestCases(
                                zsProjectKey,
                                zsPlan.get(0),
                                zsTestcasesFolderName,
                                zsTestcaseTypeName)
                        }

                    }
                }
            }
        }
    }
}
