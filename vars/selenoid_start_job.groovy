// import common.Logger

// def call() {

//     Logger logger = new Logger(this)
//     pipeline {
//         agent { node { label 'built-in' } }
//         parameters {
//             choice(name: 'build_mode', choices: ['start', 'stop'])
//             choice(name: 'browser_mode', choices: ['chrome', 'firefox', 'safari'])
//         }
//         options {
//             buildDiscarder(logRotator(daysToKeepStr: '30', numToKeepStr: '20'))
//             ansiColor('xterm')
//         }

//         stages {
//             stage('Preparing Settings') {
//                 when {
//                     environment name: 'build_mode', value: 'start'
//                 }
//                 steps {
//                     script {
//                         logger.info('Import Terraform file terraform/main.tf')
//                         File file = libraryResource 'terraform/main.tf'
//                         writeFile file: 'main.tf', text: file
//                         logger.success('ОК')

//                         logger.info('Import Terraform file terraform/variables.tf')
//                         file = libraryResource 'terraform/variables.tf'
//                         writeFile file: 'variables.tf', text: file
//                         logger.success('ОК')

//                         logger.info('Import Terraform file terraform/stackscript.sh')
//                         file = libraryResource 'terraform/stackscript.sh'
//                         writeFile file: 'stackscript.sh', text: file
//                         logger.success('ОК')

//                         logger.info('Import browsers.json')
//                         file = libraryResource 'browsers.json'
//                         writeFile file: 'browsers.json', text: file
//                     }
//                 }
//             }

//             stage('Terraform init') {
//                 when {
//                     environment name: 'build_mode', value: 'start'
//                 }
//                 steps {
//                     script {
//                         sh 'terraform init'
//                         logger.info_tf('ОК')
//                     }
//                 }
//             }

//             stage('Terraform apply') {
//                 when {
//                     environment name: 'build_mode', value: 'start'
//                 }

//                 steps {
//                     script {
//                         sh "terraform apply -var browser=${browser_mode} -auto-approve"
//                         logger.info_tf('ОК')
//                         env.ip_address = sh(returnStdout: true, script: "terraform output test_output").trim()
//                         logger.info_tf(" IP FROM TERRAFORM ${ip_address}")
//                     }
//                 }
//             }

//             stage('Terraform destroy') {
//                 when {
//                     environment name: 'build_mode', value: 'stop'
//                 }
//                 steps {
//                     script {
//                         sh 'terraform destroy -auto-approve'
//                         logger.info_tf('ОК')
//                     }
//                 }
//             }
//         }
//         post {
//             always {
//                 script {
//                     logger.success('OK')
//                 }
//             }
//         }
//     }
// }
