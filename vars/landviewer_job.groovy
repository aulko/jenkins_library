// import testrail.methods.TestPlan
// import testrail.methods.TestRun
// import testrail.methods.TestMilestone
// import testrail.methods.TestMessage
// import common.Discord
// import common.Jira
// import common.Logger
// import common.DockerCompose
// import java.util.TimeZone

// def call(job_settings) {
//     def discord = new Discord(this) as Object
//     def jira = new Jira(this) as Object
//     def docker = new DockerCompose(this) as Object
//     def tr_milestone = new TestMilestone(this) as Object
//     def tr_plan = new TestPlan(this) as Object
//     def tr_run = new TestRun(this) as Object
//     def testrail_discord = new TestMessage(this) as Object
//     def tz = TimeZone.getTimeZone("Europe/Kiev")
//     def name = "${new Date().format("MM.dd.yyyy-HH:mm", tz)} ${job_settings.name}"
//     def job_name_build_number = "\\n(JOB_NAME: ${JOB_NAME}, BUILD_NUMBER: ${BUILD_NUMBER})"
//     java.lang.Integer color_pass = 5763719
//     java.lang.Integer color_retest = 16776960



//     pipeline {
//         agent { node { label job_settings['node'] } }
//         options {
//             buildDiscarder(logRotator(daysToKeepStr: '30', numToKeepStr: '20'))
//             ansiColor('xterm')
//         }
//         environment {
//             JENKINS_UID = sh(script:"id -u", returnStdout: true).trim()
//             JENKINS_GID = sh(script:"id -g", returnStdout: true).trim()
//         }

//         stages {
//             stage('Preparing Settings') {
//                 steps {
//                     script {
//                         Logger.main(this,"Preparing domain ${job_settings.domain} ")
//                         def domain_available = sh(script:"curl -Is ${job_settings.domain} | head -n 1", returnStdout: true).trim()
//                         if (!domain_available) {
//                             Logger.error(this,"Domain ${job_settings.domain} not available")
//                             error()
//                         }

//                         Logger.main(this,"Check get browsers list [${job_settings.browsers}]")
//                         job_settings.browsers =  job_settings.browsers.split(',')
//                         job_settings.configs_id = []
//                         Logger.main(this,"Get browsers config id for testrail ${job_settings.browsers}")
//                         job_settings.browsers.each() { browser ->
//                             job_settings.configs_id.add(job_settings.testrail.browsers_config_id[browser])
//                         }


//                         Logger.main(this,"Check get suites list ${job_settings.suites}")
//                         job_settings.suites =  job_settings.suites.split(',')

//                         Logger.main(this,"Display Jenkins's build name")
//                         currentBuild.displayName ="""#${BUILD_ID} [${job_settings.domain}]"""


//                     }
//                 }
//             }
//             stage('Preparing Docker files') {
//                 steps {
//                     script {
//                         Logger.main(this,"Import Dockerfile")
//                         def file = libraryResource 'docker_images/pytest.dockerfile'
//                         writeFile file: 'Dockerfile', text: file

//                         Logger.main(this,"Import docker_compose/selenoid_csr.yml")
//                         file = libraryResource 'docker_compose/selenoid_csr.yml'
//                         writeFile file: 'docker-compose.yml', text: file

//                         Logger.main(this,"Import browsers.json")
//                         file = libraryResource 'browsers.json'
//                         writeFile file: 'browsers.json', text: file
//                     }
//                 }
//             }

//             stage('Get current Testrail Milestone id') {
//                 steps {
//                     script {
//                         Logger.main(this,"Select milestone")
//                         job_settings.tr_milestone = tr_milestone.select_milestone(
//                                 job_settings.testrail.project_id)

//                     }
//                 }
//             }

//             stage('Add Jira task') {
//                 steps {
//                     script {
//                         Logger.main(this,"Add jira task")
//                         job_settings.jira.task = jira.add_jira_task(
//                                 job_settings.tr_milestone,
//                                 name,
//                                 'Task created for Testrail Runs')
//                     }
//                 }
//             }

//             stage('Preparing Testrail') {
//                 steps {
//                     script {
//                         job_settings.testrail_runs = [:]
//                         Logger.main(this,"Create Plan with name ${name} in testrail")
//                         job_settings.testrail_plan = tr_plan.create_plan_jira(
//                                 job_settings.testrail.project_id,
//                                 name,
//                                 job_settings.tr_milestone,
//                                 job_settings.jira.task)
//                         Logger.main(this,"Discord message about start job")
//                         java.lang.String desc = "SUITES: ${job_settings.suites}\\nBROWSERS: ${job_settings.browsers}"
//                         java.lang.String title = "PLAN DESCRIPTION ${job_name_build_number}:"
//                         discord.notify_tests("[START: ${name}](${job_settings.testrail_plan.plan_url})",title, desc,color_pass, job_settings.discord)
//                         job_settings.suites.each(){suite->
//                             Logger.main(this,"Create Entry for suite [${suite}] with browser configs ${job_settings.browsers} in testrail")
//                             job_settings.testrail_runs[suite] = tr_run.create_test_entry_with_runs(
//                                     job_settings.testrail.project_id,
//                                     job_settings.testrail.suites_id[suite],
//                                     job_settings.testrail_plan.plan_id,
//                                     job_settings.configs_id,
//                                     job_settings.testrail.default_case_type,
//                                     job_settings.jira.task)
//                         }
//                         Logger.main(this,"Got runs ${job_settings.testrail_runs}")
//                     }
//                 }
//             }

//             stage('Run Tests') {
//                 steps {
//                     script {
//                         Logger.main(this, "Create images")
//                         docker.create_images(WORKSPACE, JENKINS_UID, JENKINS_GID)
//                         Logger.main(this, "Run Tests")
//                         java.lang.String title = "TEST FOR RETEST ${job_name_build_number}:"
//                         job_settings.suites.each(){suite->
//                             job_settings.testrail_runs[suite].each(){run->
//                                 def browser =  job_settings.testrail.browsers_config_id.find{ it.value == run.config_id }?.key
//                                 def run_id = run.run_id
//                                 Logger.main(this, "Start run ${run_id} for suite ${suite} with browser ${browser}")
//                                 docker.run_services(job_settings, suite, run_id, browser)
//                                 def result = testrail_discord.get_results_for_run_test_id(run_id)
//                                 Logger.main(this,"Discord message about end job with run ${run_id}")
//                                 discord.notify_tests("$name\\nEND: ${result.result}", title, result.description ,color_retest, job_settings.discord)
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//         post {
//             always {
//                 script {
//                     Logger.main(this, "close containers")
//                     docker.down_services(WORKSPACE)
//                 }
//             }
//         }
//     }
// }
