// import common.Discord
// import common.Logger
// import testrail.methods.TestMessage
// import testrail.methods.TestMilestone
// import testrail.methods.TestPlan
// import testrail.methods.TestRun
// import common.Jira
// import common.DockerCompose

// def call(jobSettings) {
//     Discord discord = new Discord(this)
//     Logger logger = new Logger(this)
//     Jira jira = new Jira(this)
//     DockerCompose docker = new DockerCompose(this)
//     TestMilestone tr_milestone = new TestMilestone(this)
//     TestPlan tr_plan = new TestPlan(this)
//     TestRun tr_run = new TestRun(this)
//     TestMessage testrail_discord = new TestMessage(this)
//     TimeZone tz = TimeZone.getTimeZone("Europe/Kiev")
//     GString name = "${new Date().format("MM.dd.yyyy-HH:mm", tz)} ${job_settings.name} (${job_settings.browser})"
//     GString job_name_build_number = "\\n(JOB_NAME: ${JOB_NAME}, BUILD_NUMBER: ${BUILD_NUMBER})"
//     Integer color_pass = 5763719
//     Integer color_retest = 16776960



//     pipeline {
//         agent { node { label job_settings['node'] } }
//         options {
//             buildDiscarder(logRotator(daysToKeepStr: '30', numToKeepStr: '20'))
//             ansiColor('xterm')
//         }
//         environment {
//             JENKINS_UID = sh(script:"id -u", returnStdout: true).trim()
//             JENKINS_GID = sh(script:"id -g", returnStdout: true).trim()
//         }
//         stages {
//             stage('Preparing Settings') {
//                 steps {
//                     script {
//                         logger.info("Preparing domain ${job_settings.domain} ")
//                         def domain_available = sh(script:"curl -Is ${job_settings.domain} | head -n 1", returnStdout: true).trim()
//                         if (!domain_available) {
//                             logger.error("Domain ${job_settings.domain} not available")
//                             error()
//                         }
//                         logger.success("OK")

//                         logger.info("Get languages list [${job_settings.languages}]")
//                         String languages = job_settings.languages
//                         job_settings.languages = languages.split(',')
//                         job_settings.configs_id = []
//                         logger.success("OK")
//                         logger.info("Get languages config id for testrail ${job_settings.languages}")
//                         job_settings.languages.each() {language ->
//                             job_settings.configs_id.add(job_settings.testrail.languages_config_id[language as String])
//                         }
//                         logger.success("OK")
//                         logger.info("Get deafault language config id for testrail ${job_settings.testrail.default_language}")
//                         job_settings.default_configs_id = job_settings.testrail.languages_config_id[job_settings.default_language as String]
//                         logger.success("OK")

//                         logger.info("Check get suites list ${job_settings.suites}")
//                         String suites = job_settings.suites
//                         job_settings.suites = suites.split(',')
//                         logger.success("OK")
//                         logger.info("Display Jenkins's build name")
//                         currentBuild.displayName ="#${BUILD_ID} [${job_settings.domain}]"
//                         logger.success("OK")
//                         logger.info("Preparing Selenoid")
//                         def result = build job: 'selenoid_start', parameters: [
//                                 string(name: 'build_mode', value: 'start'),
//                                 string(name: 'browser_mode', value: "${job_settings.browser}")]
//                         job_settings.ip_address = result.buildVariables.get('ip_address')
//                         logger.success("OK")
//                     }
//                 }
//             }
//             stage('Preparing Docker files') {
//                 steps {
//                     script {
//                         logger.info("Import Dockerfile")
//                         def file = libraryResource 'docker_images/pytest.dockerfile'
//                         writeFile file: 'Dockerfile', text: file

//                         logger.info("Import docker_compose/python_project.yml")
//                         file = libraryResource 'docker_compose/python_project.yml'
//                         writeFile file: 'docker-compose.yml', text: file

//                         logger.info("Import browsers.json")
//                         file = libraryResource 'browsers.json'
//                         writeFile file: 'browsers.json', text: file
//                     }
//                 }
//             }

//             stage('Get current Testrail Milestone id') {
//                 steps {
//                     script {
//                         logger.info_tr("Select milestone for project id ${job_settings.testrail.project_id}")
//                         job_settings.tr_milestone = tr_milestone.select_milestone(job_settings.testrail.project_id)
//                         logger.info_tr("Select sub milestone by milestone id ${job_settings.tr_milestone}")
//                         job_settings.tr_sub_milestone = tr_milestone.select_sub_milestone(job_settings.tr_milestone.milestone_id,
//                                 job_settings.testrail.sub_milestone_name)
//                     }
//                 }
//             }

//             stage('Preparing Jira') {
//                 steps {
//                     script {
//                         logger.info_jira("Add jira task")
//                         job_settings.jira.task = jira.add_jira_bug(
//                                 job_settings.tr_milestone.milestone_id,
//                                 job_settings.tr_milestone.milestone_name,
//                                 'Task created for Testrail Runs')
//                         logger.info_jira("Add jira sub task for testing type")
//                         job_settings.jira.subtask = jira.create_jira_sub_task(
//                                 name, 'Task created for Testrail Runs', job_settings.jira.component_name, job_settings.jira.task)
//                     }
//                 }
//             }

//             stage('Preparing Testrail') {
//                 steps {
//                     script {
//                         job_settings.testrail_runs = [:]
//                         logger.info_tr("Create Plan with name ${name} in testrail")
//                         job_settings.testrail_plan = tr_plan.create_plan_jira(job_settings.testrail.project_id,
//                                 name, job_settings.tr_sub_milestone)
//                         logger.info_dis("Discord message about start job")
//                         GString desc = "SUITES: ${job_settings.suites}\\nLANGUAGES: ${job_settings.languages}"
//                         GString title = "PLAN DESCRIPTION ${job_name_build_number}:"
//                         discord.notify_tests("""[START: ${name}](${job_settings.testrail_plan.plan_url})""",title, desc,color_pass, job_settings.discord.webhook_channel)
//                         job_settings.suites.each(){suite->
//                             logger.info_tr("Create entry for suite [${suite}] with languages ${job_settings.languages} in testrail")
//                             job_settings.testrail_runs[suite as String] = tr_run.create_test_entry_with_runs(
//                                     job_settings.testrail.project_id,
//                                     job_settings.testrail.suites_id[suite as String],
//                                     job_settings.testrail_plan.plan_id,
//                                     job_settings.configs_id,
//                                     job_settings.testrail.case_type,
//                                     job_settings.default_configs_id,
//                                     job_settings.jira.subtask
//                             )
//                         }
//                         logger.success("OK")
//                     }
//                 }
//             }

//             stage('Run Tests') {
//                 steps {
//                     script {
//                         logger.info_doc("Create images")
//                         docker.create_images(WORKSPACE)
//                         GString title = "REPORT TESTING RESULT ${job_name_build_number}:"
//                         job_settings.suites.each(){suite->
//                             job_settings.testrail_runs[suite as String].each(){run->
//                                 def language =  job_settings.testrail.languages_config_id.find{ it.value == run.config_id }?.key
//                                 logger.success("language ---- ${language}")
//                                 def run_id = run.run_id
//                                 logger.info_doc("Start run ${run_id} for suite ${suite} with language ${language}")
//                                 if (language == job_settings.default_language) {
//                                     docker.run_services(job_settings, suite, run_id, language, false)}
//                                 else{
//                                     docker.run_services(job_settings, suite, run_id, language, true)}
//                                 logger.info_dis("Discord message about end job with run ${run_id}")
//                                 def result = testrail_discord.get_results_for_run_test_id(run_id)
//                                 if (result.description) {
//                                     discord.notify_tests("$name\\nEND: ${result.result}", title, result.description, color_retest, job_settings.discord.webhook_channel)}
//                                 else {
//                                     discord.notify_tests("$name\\nEND: ${result.result}", title, result.description, color_pass, job_settings.discord.webhook_channel)}
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//         post {
//             always {
//                 script {
//                     build job: 'selenoid_start', parameters: [string(name: 'build_mode', value: 'stop')]
//                     logger.info_doc("Close containers")
//                     docker.down_services(WORKSPACE)
//                 }
//             }
//         }
//     }
// }
