// import testrail.methods.TestPlan
// import testrail.methods.TestRun
// import testrail.methods.TestMilestone
// import testrail.methods.TestMessage
// import common.Discord
// import common.Jira
// import common.Logger
// import common.DockerCompose


// def call(job_settings) {
//     Discord  discord = new Discord(this)
//     Jira jira = new Jira(this)
//     DockerCompose docker = new DockerCompose(this)
//     TestMilestone tr_milestone = new TestMilestone(this)
//     TestPlan tr_plan = new TestPlan(this)
//     TestRun tr_run = new TestRun(this)
//     TestMessage testrail_discord = new TestMessage(this)
//     TimeZone tz = TimeZone.getTimeZone("Europe/Kiev")
//     GString name = "${new Date().format("MM.dd.yyyy-HH:mm", tz)} ${job_settings.name}"
//     GString job_name_build_number = "\\n(JOB_NAME: ${JOB_NAME}, BUILD_NUMBER: ${BUILD_NUMBER})"
//     Integer color_pass = 5763719
//     Integer color_retest = 16776960



//     pipeline {
//         agent { node { label job_settings['node'] } }
//         options {
//             buildDiscarder(logRotator(daysToKeepStr: '30', numToKeepStr: '20'))
//             ansiColor('xterm')
//         }
//         environment {
//             JENKINS_UID = sh(script:"id -u", returnStdout: true).trim()
//             JENKINS_GID = sh(script:"id -g", returnStdout: true).trim()
//         }
//         stages {
//             stage('Preparing Settings') {
//                 steps {
//                     script {
//                         logger.info("Preparing domain ${job_settings.domain} ")
//                         def domain_available = sh(script:"curl -Is ${job_settings.domain} | head -n 1", returnStdout: true).trim()
//                         if (!domain_available) {
//                             error()
//                         }
//                         logger.success("OK")
//                         logger.info("Check suites list ${job_settings.suites}")
//                         String suites = job_settings.suites
//                         job_settings.suites = suites.split(',')
//                         logger.success("OK")
//                         logger.info("Check number_of_users ${job_settings.locust.number_of_users}")
//                         logger.success("OK")
//                         logger.info("Check spawn_rate ${job_settings.locust.spawn_rate}")
//                         logger.success("OK")
//                         logger.info("Check time_limit ${job_settings.locust.time_limit}")
//                         logger.success("OK")
//                         logger.info("Check workers ${job_settings.locust.workers}")
//                         logger.success("OK")
//                         logger.info("Display Jenkins's build name")
//                         currentBuild.displayName ="""#${BUILD_ID} [${job_settings.domain}]"""
//                         logger.success("OK")
//                     }
//                 }
//             }
//             stage('Preparing Docker file') {
//                 steps {
//                     script {
//                         logger.info("Import docker_compose/locust.yml")
//                         file = libraryResource 'docker_compose/locust.yml'
//                         writeFile file: 'docker-compose.yml', text: file

//                         logger.info("Import docker_images/locust.dockerfile")
//                         file = libraryResource 'docker_images/locust.dockerfile'
//                         writeFile file: 'Dockerfile', text: file
//                     }
//                 }
//             }


//             stage('Get current Testrail Milestone id') {
//                 steps {
//                     script {
//                         logger.info("Select milestone")
//                         job_settings.tr_milestone = tr_milestone.select_milestone(job_settings.testrail.project_id)
//                         logger.info("Select sub milestone")
//                         job_settings.tr_sub_milestone = tr_milestone.select_sub_milestone(
//                                 job_settings.tr_milestone,
//                                 job_settings.testrail.sub_milestone_name)
//                     }
//                 }
//             }

//             stage('Add Jira task') {
//                 steps {
//                     script {
//                         logger.info("Add jira task to milestone")
//                         job_settings.jira.task = jira.add_jira_task(
//                                 job_settings.tr_milestone,
//                                 name,
//                                 'Task created for Testrail')
//                         logger.info("Add jira task to sub milestone")
//                         job_settings.jira.task = jira.add_jira_task_to_sub_milestone(
//                                 job_settings.tr_sub_milestone)
//                     }
//                 }
//             }

//             stage('Preparing Testrail') {
//                 steps {
//                     script {
//                         job_settings.testrail_runs = [:]
//                         logger.info("Create Plan with name ${name} in testrail")
//                         job_settings.testrail_plan = tr_plan.create_plan_jira(
//                                 job_settings.testrail.project_id,
//                                 name,
//                                 job_settings.tr_sub_milestone)
//                         logger.info("Discord message about start job")
//                         GString desc = "SUITES: ${job_settings.suites}"
//                         GString title = "PLAN DESCRIPTION ${job_name_build_number}:"
//                         discord.notify_tests("[START: ${name}](${job_settings.testrail_plan.plan_url})",title, desc,color_pass, job_settings.discord)
//                         job_settings.suites.each(){suite->
//                             logger.info("Create Entry for suite [${suite}] with browser configs ${job_settings.browsers} in testrail")
//                             job_settings.testrail_runs[suite as String] = tr_run.create_test_entry_with_runs(
//                                     job_settings.testrail.project_id,
//                                     job_settings.testrail.suites_id[suite as String],
//                                     job_settings.testrail_plan.plan_id,
//                                     job_settings.configs_id,
//                                     job_settings.testrail.default_case_type,
//                                     job_settings.default_configs_id,
//                                     job_settings.jira.task)
//                         }
//                         logger.info("Got runs ${job_settings.testrail_runs}")
//                     }
//                 }
//             }

//             stage('Run Tests') {
//                 steps {
//                     script {
//                         logger.info("Create images")
//                         docker.create_images(WORKSPACE)
//                         logger.info("Run Tests")
//                         String title = "TEST FOR RETEST ${job_name_build_number}:"
//                         job_settings.suites.each() { suite ->
//                             job_settings.testrail_runs[suite as String].each() { run ->
//                                 def browser = job_settings.testrail.browsers_config_id.find { it.value == run.config_id }?.key
//                                 def run_id = run.run_id
//                                 logger.info( "Start run ${run_id} for suite ${suite} with browser ${browser}")
//                                 docker.run_locust(job_settings, run_id)
//                                 def result = testrail_discord.get_results_for_run_test_id(run_id)
//                                 logger.info("Discord message about end job with run ${run_id}")
//                                 discord.notify_tests("$name\\nEND: ${result.result}", title, result.description, color_retest, job_settings.discord)
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//         post {
//             always {
//                 script {
//                     Logger.main(this, "close containers")
//                     docker.down_services(WORKSPACE)
//                 }
//             }
//         }
//     }
// }
