package zephyr_scale

import common.Logger
import groovy.json.JsonSlurperClassic

/* groovylint-disable-next-line ClassJavadoc, CompileStatic */
class ZephyrScaleMethod {

    static final String API_AUTH = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb250ZXh0Ijp7ImJhc2VVcmwiOiJodHRwczovL3Rlc3RyYWlsdGVzdC5hdGxhc3NpYW4ubmV0IiwidXNlciI6eyJhY2NvdW50SWQiOiI1YmQxODVmMWQ1NzVkODNmYTM1YThmMGMifX0sImlzcyI6ImNvbS5rYW5vYWgudGVzdC1tYW5hZ2VyIiwic3ViIjoiMmI1MGVlYTYtZjU2NS0zMDQxLTlhYTktZTI2ODExZjljZjg3IiwiZXhwIjoxNzIwMjA1MjI5LCJpYXQiOjE2ODg2NjkyMjl9.SxPLgPnCLoP2LmZyf7kPrBP5Et2fe_J1aRrM3jRSbRA'
    static final String API_HOST = 'https://api.zephyrscale.smartbear.com/v2'
    static final ArrayList STATUS_SUCCESS = [200, 201]

    Object script
    Logger logger

    /* groovylint-disable-next-line MethodParameterTypeRequired, NoDef */
    ZephyrScaleMethod(script) {
        this.script = script
        this.logger  = new Logger(this.script) }

    Object sendRequest(String url, String data) {
        URLConnection conn = url.toURL().openConnection()
        conn.setRequestProperty('Authorization', API_AUTH)
        if (data != '') {
            conn.setRequestProperty('Content-Type', 'application/json')
            conn.setDoOutput(true)
            conn.getOutputStream().write(data.getBytes('UTF-8'))
        }
        Integer statusCode = conn.responseCode
        if (STATUS_SUCCESS.contains(statusCode)) {
            logger.success("${statusCode}")
            Object result = new JsonSlurperClassic().parseText(conn.content.text as String)
            return result }
        logger.error("${statusCode}")
        script.error()
        return null
    }

    String getLastReleaseFolder() {
        logger.infoZs('Select release folder')
        GString url = "${API_HOST}/folders?folderType=TEST_CYCLE"
        Object foldersTestCycle = sendRequest(url, '').values
        ArrayList listReleaseFolderIds = []
        foldersTestCycle.each { folderTestCycle ->
            if (folderTestCycle.parentId == null) {
                listReleaseFolderIds.add(folderTestCycle.id)
            }
        }
        return listReleaseFolderIds.sort().reverse().get(0).toString()
    }

    String getTestTypeFolder(String projectKey, String releaseFolderId, String testTypeFolderName) {
        logger.infoZs("""Select testType folder - projectKey <${projectKey}>, releaseFolderId <${releaseFolderId}>,
                         testTypeFolderName <${testTypeFolderName}>""")
        GString url = "${API_HOST}/folders?folderType=TEST_CYCLE"
        Object foldersTestCycle = sendRequest(url, '').values
        Integer testTypeFolderId = 0
        foldersTestCycle.each { folderTestCycle ->
            if (folderTestCycle.name == testTypeFolderName) {
                testTypeFolderId = folderTestCycle.id
            }
        }
        if (testTypeFolderId == 0) {
            url = "${API_HOST}/folders"
            GString data = """{"parentId": "${releaseFolderId}", "name": "${testTypeFolderName}",
                               "projectKey": "${projectKey}", "folderType": "TEST_CYCLE"}"""
            foldersTestCycle = sendRequest(url, data)
            testTypeFolderId = foldersTestCycle.id
        }
        return testTypeFolderId.toString()
    }

    String createPlanFolder(String projectKey, String testTypeFolderId, String planFolderName) {
        logger.infoZs("""Create plan folder - projectKey <${projectKey}>, testTypeFolderId <${testTypeFolderId}>,
                         planFolderName <${planFolderName}>""")
        GString url = "${API_HOST}/folders"
        GString data = """{"parentId": "${testTypeFolderId}", "name": "${planFolderName}",
                           "projectKey": "${projectKey}", "folderType": "TEST_CYCLE"}"""
        Object foldersTestCycle = sendRequest(url, data)
        return foldersTestCycle.id.toString()
    }

    Object addTestCycle(String projectKey, String planFolderId, ArrayList browsers) {
        logger.infoZs("""Create testCycles - projectId <${projectKey}>,  planFolderId - <${planFolderId}>,
                           browsers <${browsers}>""")
        Object result = []
        browsers.each { browser ->
            GString url = "${API_HOST}/testcycles"
            GString data = """{"projectKey": "${projectKey}", "name": "${browser}", "folderId": "${planFolderId}"}"""
            Object testCycle = sendRequest(url, data)
            result.add([testCycle.key, browser])
        }
        return result
    }

    List getTestCasesFolder(String testCasesFolderName) {
        logger.infoZs("Select testcases folder - testCasesFolderName <${testCasesFolderName}>")
        GString url = "${API_HOST}/folders?folderType=TEST_CASE"
        Object foldersTestCases = sendRequest(url, '').values
        ArrayList listTestCasesFolderId = []
        foldersTestCases.each { folderTestCases ->
            if (testCasesFolderName.equals(folderTestCases.name)) {
                listTestCasesFolderId.add(folderTestCases.id.toString())
            }
        }
        foldersTestCases.each { folderTestCases ->
            String parentsTestCasesFolderId = listTestCasesFolderId.get(0)
            if (parentsTestCasesFolderId.equals(folderTestCases.parentId.toString())) {
                listTestCasesFolderId.add(folderTestCases.id.toString())
            }
        }
        return listTestCasesFolderId
    }

    Object getAmountTestCaseSteps(ArrayList listTestCasesKey) {
        logger.infoZs("Add amount steps to testcase - listTestCasesKey <${listTestCasesKey}>")
        GString url
        ArrayList listTestCasesKeyWithAmountSteps = []
        listTestCasesKey.each { testCaseKey ->
            url = "${API_HOST}/testcases/${testCaseKey}/teststeps"
            Object teststeps = sendRequest(url, '').values
            listTestCasesKeyWithAmountSteps.add([testCaseKey, teststeps.size()])
        }
        return listTestCasesKeyWithAmountSteps
    }

    Object getTestCasesByTestTypeAutomationFalse(String testCasesFolderName, String caseTypeName) {
        logger.infoZs("Select testcases - testCasesFolderName <${testCasesFolderName}>, caseTypeName <${caseTypeName}>")
        ArrayList listTestCasesFolderId = getTestCasesFolder(testCasesFolderName)
        ArrayList listTestCasesKey = []
        GString url
        listTestCasesFolderId.each { testCasesFolderId ->
            url = "${API_HOST}/testcases?folderId=${testCasesFolderId}"
            Object testCases = sendRequest(url, '').values
            testCases.each { testCase ->
                if (testCase.customFields.Type == caseTypeName && !testCase.customFields.Automation) {
                    listTestCasesKey.add(testCase.key)
                }
            }
        }
        return getAmountTestCaseSteps(listTestCasesKey)
    }

    Object createExecutionWithTestCases(String projectKey, String testCycleKey, String testCasesFolderName,
                                        String caseTypeName) {
        logger.infoZs("""Add execution - projectKey <${projectKey}>, testCycleKey <${testCycleKey}>,
                         testCasesFolderName <${testCasesFolderName}>, caseTypeName <${caseTypeName}>""")
        ArrayList listTestCasesWithAmountSteps = getTestCasesByTestTypeAutomationFalse(testCasesFolderName, caseTypeName)
        String statusName =  'Not Executed'
        GString url = "${API_HOST}/testexecutions"
        String stepsAmount
        String testCaseKey
        listTestCasesWithAmountSteps.each { testCasesWithAmountSteps ->
            testCaseKey = testCasesWithAmountSteps.get(0)
            stepsAmount = testCasesWithAmountSteps.get(1)
            ArrayList testScriptResults = []
            for (int i = 0; i < stepsAmount.toInteger(); i++) {
                testScriptResults.add("""{"statusName": "${statusName}"}""")
            }
            GString data = """{"projectKey": "${projectKey}", "testCaseKey": "${testCaseKey}",
                               "testCycleKey": "${testCycleKey}", "statusName": "${statusName}"},
                               "testScriptResults": "${testScriptResults}"}"""
            Object testExecution = sendRequest(url, data)
        }
    }
}