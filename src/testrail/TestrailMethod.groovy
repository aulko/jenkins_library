package testrail

import common.Logger
import groovy.json.JsonSlurperClassic

/* groovylint-disable-next-line ClassJavadoc, CompileStatic */
class TestrailMethod {

    static final String API_AUTH = 'Basic ZW9zZGF0ZXN0czIwMjJAZ21haWwuY29tOjExRTA1bnVmIQ=='
    static final String API_HOST = 'https://kouo3.testrail.io/index.php?'
    static final Integer STATUS_SUCCESS = 200

    Object script
    Logger logger

    /* groovylint-disable-next-line MethodParameterTypeRequired, NoDef */
    TestrailMethod(script) {
        this.script = script
        this.logger  = new Logger(this.script) }

    Object sendRequest(String url, String data) {
        URLConnection conn = url.toURL().openConnection()
        conn.setRequestProperty('Authorization', API_AUTH)
        if (data != '') {
            conn.setRequestProperty('Content-Type', 'application/json')
            conn.setDoOutput(true)
            conn.getOutputStream().write(data.getBytes('UTF-8'))
        }
        Integer statusCode = conn.responseCode
        if (statusCode == STATUS_SUCCESS) {
            logger.success("${statusCode}")
            Object result = new JsonSlurperClassic().parseText(conn.content.text as String)
            return result }
        logger.error("${statusCode}")
        script.error()
        return null
    }

    Object getLastMilestoneNotComplete(String projectId) {
        logger.infoTr("Select milestone - projectId <${projectId}>")
        GString url = "${API_HOST}/api/v2/get_milestones/${projectId}&is_completed=0"
        Object milestones = sendRequest(url, '').milestones
        Integer  milestonesSize = milestones.size() - 1
        Object milestone = milestones.get(milestonesSize)
        return [milestone.id.toString(), milestone.name]
    }

    Object getSubMilestone(String projectId, String milestoneId, String subMilestoneName) {
        logger.infoTr("""Select submilestone - projectId <${projectId}>,  milestoneId - <${milestoneId}>,
                         subMilestoneName <${subMilestoneName}>""")
        GString url = "${API_HOST}/api/v2/get_milestone/${milestoneId}"
        Object subMilestones = sendRequest(url, '').milestones
        Integer subMilestoneId = 0
        subMilestones.each { subMilestone ->
            if (subMilestone.name == subMilestoneName) {
                subMilestoneId = subMilestone.id
            }
        }
        if (subMilestoneId == 0) {
            url = "${API_HOST}/api/v2/add_milestone/${projectId}"
            GString data = """{"name": "${subMilestoneName}", "parent_id": "${milestoneId}"}"""
            Object subMilestone = sendRequest(url, data)
            subMilestoneId = subMilestone.id
        }
        return subMilestoneId.toString()
    }

    Object addPlan(String projectId, String subMilestoneId, String name) {
        logger.infoTr("Create plan - projectId <${projectId}>,  subMilestoneId - <${subMilestoneId}>, name <${name}>")
        GString url = "${API_HOST}/api/v2/add_plan/${projectId}"
        GString data = """{"name": "${name}", "milestone_id": "${subMilestoneId}"}"""
        Object plan = sendRequest(url, data)
        return [plan.id, plan.url]
    }

    ArrayList getCasesByType(String projectId, String suiteId, String caseTypesIds) {
        logger.infoTr("Select cases - projectId <${projectId}>, suiteId <${suiteId}>, caseTypesId <${caseTypesIds}>")
        GString url = "${API_HOST}/api/v2/get_cases/${projectId}&suite_id=${suiteId}&type_id=${caseTypesIds}"
        Object cases = sendRequest(url, '').cases
        ArrayList listCasesId = []
        cases.each { singleCase ->
            listCasesId.add(singleCase.id.toString())
        }
        return listCasesId
    }

    Object createEntryWithRunsCases(String projectId, String suiteId, String planId, ArrayList configIds, 
                                    String caseTypesIds) {
        ArrayList listCasesId = getCasesByType(projectId, suiteId, caseTypesIds)
        logger.infoTr("""Create entry - projectId <${projectId}>, suiteId <${suiteId}>, planId <${planId}>,
                         configsIds <${configIds}>,  caseTypesIds <${caseTypesIds}>""")
        ArrayList runs = []
        GString url = "${API_HOST}/api/v2/add_plan_entry/${planId}"
        configIds.each { configId ->
            runs.add("""{"include_all": false, "case_ids": ${listCasesId}, "config_ids": [${configId}]}""")
        }
        GString data = """{"suite_id": ${suiteId}, "include_all": true, "config_ids": ${configIds}, "runs": ${runs}}"""
        Object entry = sendRequest(url, data)
        ArrayList result = []
        entry.runs.each { run ->
            result.add([run.id, run.config_ids.get(0)])
        }
        return result
    }

}
