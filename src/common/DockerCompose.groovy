// package common

// class DockerCompose {
//     def script
//     DockerCompose(script) {this.script = script}

//     def create_images(workspace){
//         def logger = new Logger(this.script)
//         script.sh "docker-compose --file ${workspace}/docker-compose.yml up --build --no-start"
//         logger.success("OK")
//     }

//     def down_services(workspace){
//         def logger = new Logger(this.script)
//         try {
//             script.sh "docker-compose --file ${workspace}/docker-compose.yml down"
//             logger.success("Delete containers OK")
//         }
//         catch (Exception e){
//             def error = e.toString()
//             logger.error("Delete containers ERROR ${error}")
//         }
//     }

//     def run_services(job_settings, test_section, test_run_id, language, locality){
//         def logger = new Logger(this.script)
//         logger.success("Run containers OK")
//         def test_group = job_settings.pytest.test_group
//         try {
//             if (locality){
//                 test_group = "${test_group} and locality"
//             }
//             script.sh "docker-compose run" +
//                             " -e run=${test_run_id}" +
//                             " -e browser=${job_settings.browser}" +
//                             " -e domain=${job_settings.domain}" +
//                             " -e jira_task=${job_settings.jira.subtask}" +
//                             " -e selenoid=${job_settings.ip_address}" +
//                             " -e user=${job_settings.user}" +
//                             " -e language=${language}" +
//                             " python pytest ${job_settings.pytest.test_path}  --dist=loadscope -v -n ${job_settings.pytest.threads} -m" +
//                             " \"${test_section} and ${test_group}\""
//             }
//         catch (Exception e){
//             def error = e.toString()
//             logger.error("Run containers ERROR ${error}")
//         }
//     }

//     def run_locust(job_settings, test_run_id){
//         def logger = new Logger(this.script)
//         logger.success("Run containers OK")
//         try {
//             script.sh "docker-compose run" +
//                     " -e run=${test_run_id}" +
//                     " -e domain=${job_settings.domain}" +
//                     " -e users_count=${job_settings.locust.number_of_users}" +
//                     " -e spawn_rate=${job_settings.locust.spawn_rate}" +
//                     " -e time_limit=${job_settings.locust.time_limit}" +
//                     " locust_exist -f /mnt/locust/scenario_cart.py --headless --html /home/locust/report/locust_report.html"
//         }
//         catch (Exception e){
//             def error = e.toString()
//             logger.error("Run containers ERROR ${error}")
//         }
//     }
// }
