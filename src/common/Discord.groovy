// package common

// class Discord{
//     def server
//     Discord(server) {this.server = server}

//     def api = 'https://discord.com/api/'
//     def avatar = "https://www.jenkins.io/images/logos/formal/formal.png"

//     def notify_tests(content, title, description, color, webhook_channel) {
//         def logger = new Logger(this.server)
//             GString url = "${api}${webhook_channel}"
//             URLConnection conn = url.toURL().openConnection()
//             GString data = """{
//                                 "avatar_url": "${avatar}",
//                                 "content": "${content}",
//                                 "username": "Jenkins",
//                                 "embeds": [
//                                     {
//                                         "title": "${title}",
//                                         "description": "${description}",
//                                         "color": ${color}
//                                     }
//                                 ]
//                                 }"""
//             conn.setRequestProperty("Content-Type", "application/json")
//             conn.setDoOutput(true)
//             conn.getOutputStream().write(data.getBytes("UTF-8"))
//             def status_code = conn.responseCode
//             if (status_code == 204) {
//                 logger.success("Status code ${status_code}")}
//             else{
//                 logger.error("Status code ${status_code}")}
//     }
// }