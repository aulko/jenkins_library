/* groovylint-disable NoDef */
package common

/* groovylint-disable-next-line ClassJavadoc, CompileStatic */
class Logger {

    Object script

    /* groovylint-disable-next-line MethodParameterTypeRequired */
    Logger(script) { this.script = script }

    void error(String message) {
        script.echo "\u001B[1;31m [ERROR] ${message} \u001B[0m"
    }

    void info(String message) {
        script.echo "\u001B[1;34m [INFO] <<<PREPARING>>> ${message} \u001B[0m"
    }

    void infoTr(String message) {
        script.echo "\u001B[1;34m [INFO] <<<TESTRAIL>>> ${message} \u001B[0m"
    }

    void infoZs(String message) {
        script.echo "\u001B[1;34m [INFO] <<<ZEPHYR SCALE>>> ${message} \u001B[0m"
    }

    void infoDis(String message) {
        script.echo "\u001B[1;34m [INFO] <<<DISCORD>>> ${message} \u001B[0m"
    }

    void infoDoc(String message) {
        script.echo "\u001B[1;34m [INFO] <<<DOCKER>>> ${message} \u001B[0m"
    }

    void infoJira(String message) {
        script.echo "\u001B[1;34m [INFO] <<<JIRA>>> ${message} \u001B[0m"
    }

    void infoTf(String message) {
        script.echo "\u001B[1;34m [INFO] <<<TERRAFORM>>> ${message} \u001B[0m"
    }

    void success(String message) {
        script.echo "\u001B[1;32m [SUCCESS] ${message} \u001B[0m"
    }

}