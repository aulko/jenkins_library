package common

/* groovylint-disable-next-line ClassJavadoc, CompileStatic */
class CommonMethod {

    Object script
    Logger logger

    /* groovylint-disable-next-line MethodParameterTypeRequired, NoDef */
    CommonMethod(script) {
        this.script = script
        this.logger  = new Logger(this.script)}

    Object CutObjectValueByList(Object oldObject, ArrayList arrayValue){
        Object newObject = [:]
        arrayValue.each { value ->
            newObject.put(value, oldObject[value])
        }
        return newObject
    }

    Object getValuesFromObject(Object oldObject) {
        ArrayList newList = []
        oldObject.each { key, value ->
            newList.add(value)
        }
        return newList
    }
}
