// package common
// import groovy.json.JsonSlurperClassic
// import testrail.api_requests.Milestone

// class Jira{
//     def script
//     Jira(script) {this.script = script}

//     String auth_jira = 'Basic em1leWtvODFAZ21haWwuY29tOkF0YUh0Tm03R1JlaDdlOVpEZk9GQkJCQQ=='
//     String bug_id = '10007'
//     String sub_task_id = '10008'
//     String project_id = '10001'

//     def create_jira_bug(title, description) {
//         def logger = new Logger(this.script)
//         String end_point = 'https://testrailtest.atlassian.net/rest/api/3/issue'
//         URLConnection conn = end_point.toURL().openConnection()
//         GString data = """{
//                                 "fields": {
//                                     "summary": "${title}",
//                                     "issuetype": {"id": "${bug_id}"},
//                                     "project": {"id": "${project_id}"},
//                                     "description": {
//                                         "type": "doc",
//                                         "version": 1,
//                                         "content": [
//                                             {
//                                                 "type": "paragraph",
//                                                 "content": [
//                                                     {"text": "${description}", "type": "text"}
//                                                 ]
//                                             }
//                                         ]
//                                     }
//                                 }
//                             }"""
//         conn.setRequestProperty("Content-Type", "application/json")
//         conn.setRequestProperty("Authorization", auth_jira)
//         conn.setDoOutput(true)
//         conn.getOutputStream().write(data.getBytes("UTF-8"))
//         def status_code = conn.responseCode
//         if (status_code == 201) {
//             def response_json = new JsonSlurperClassic().parseText(conn.content.text as String)
//             logger.success("Status code creating jira bug ${status_code}")
//             return response_json}
//         else{
//             logger.error("Status code creating jira bug ${status_code}")}
//     }

//     def create_jira_sub_task(title, description, component, parent_task) {
//         def logger = new Logger(this.script)
//         String end_point = 'https://testrailtest.atlassian.net/rest/api/3/issue'
//         URLConnection conn = end_point.toURL().openConnection()
//         GString data = """{
//                          "update": {
//                                 "components": [
//                                     {
//                                         "set": [{ "name": "${component}"}]
//                                     }
//                                 ]
//                             },
//                             "fields": {
//                                 "summary": "${title}",
//                                 "issuetype": {"id": "${sub_task_id}"},
//                                 "project": {"id": "${project_id}"},
//                                  "parent":{"key": "${parent_task}"},
//                                 "description": {
//                                     "type": "doc",
//                                     "version": 1,
//                                     "content": [
//                                         {
//                                             "type": "paragraph",
//                                             "content": [
//                                                 {"text": "${description}", "type": "text"}
//                                             ]
//                                         }
//                                     ]
//                                 }
//                             }
//                         }"""
//         conn.setRequestProperty("Content-Type", "application/json")
//         conn.setRequestProperty("Authorization", auth_jira)
//         conn.setDoOutput(true)
//         conn.getOutputStream().write(data.getBytes("UTF-8"))
//         def status_code = conn.responseCode
//         if (status_code == 201) {
//             def response_json = new JsonSlurperClassic().parseText(conn.content.text as String)
//             logger.success("Status code creating jira sub task ${status_code}")
//             return response_json.key}
//         else{
//             logger.error("Status code creating jira sub task ${status_code}")}
//     }

//     def add_jira_bug(milestone_id, title, description) {
//         def logger = new Logger(this.script)
//         def milestone = new Milestone(this.script)
//         def current_milestone = milestone.get_milestone_by_id(milestone_id)
//         if (current_milestone.refs){
//             logger.success("Reference exist ${current_milestone.refs}")
//             return current_milestone.refs
//         }
//         else{
//             def jira_task = this.create_jira_bug(title, description)
//             logger.success("Bug created")
//             milestone.update_milestone_by_id(milestone_id, jira_task.key)
//             return jira_task.key
//         }
//     }
// }
