FROM python:3.8

COPY ./requirements.txt /tmp/requirements.txt
RUN python -m pip install --upgrade pip==22.3.1 && pip install -r /tmp/requirements.txt