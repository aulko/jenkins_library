variable "token" {
  type = string
  default = "9f21a6b66d68bcb1d53017517d3425c68519dc660cba6b61e97d9fdce4f2541b"
}

variable "authorized_keys" {
  type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAA"
}

variable "root_password" {
  type = string
  default = "202320E05nuf!"
}

variable "browser" {
  type = string
  default = "chrome"
}

variable "browsers" {
  default = {
    "chrome": "selenoid/chrome:106.0"
    "firefox": "selenoid/firefox:106.0"
    "safari": "browsers/safari:15.0"
  }
}