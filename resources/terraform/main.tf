terraform {
  required_providers {
    linode= {
      source = "linode/linode"
      version = "1.30.0"
    }
  }
}

provider "linode" {
  token = var.token
}

resource "linode_stackscript" "selenoid" {
  label = "selenoid"
  description = "Install for selenoid"
  script = file("stackscript.sh")
  images = ["linode/ubuntu18.04"]
  rev_note = "initial version"
}

resource "linode_instance" "selenoid" {
  image = "linode/ubuntu18.04"
  label = "selenoid"
  region = "eu-central"
  type = "g6-standard-2"
  authorized_keys = [ var.authorized_keys ]
  root_pass = var.root_password
  stackscript_id = linode_stackscript.selenoid.id
  connection {
    type     = "ssh"
    user     = "root"
    password = var.root_password
    host        = linode_instance.selenoid.ip_address
  }
  provisioner "file" {
    source      = "browsers.json"
    destination = "browsers.json"
  }

  provisioner "remote-exec" {
    inline = [<<EOT
        while (! docker stats --no-stream ); do
          echo "Waiting for Docker to launch..."
          sleep 10
        done
       EOT
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "docker pull aerokube/selenoid:latest",
      "docker pull ${lookup(var.browsers, var.browser)}",
      "docker run -d --name selenoid -p 4444:4444 -v /var/run/docker.sock:/var/run/docker.sock -v $PWD/:/etc/selenoid/:ro aerokube/selenoid:latest"
    ]
  }
}

output "test_output" {
  value = linode_instance.selenoid.ip_address
}